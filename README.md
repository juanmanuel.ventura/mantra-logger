Sever and HTTP logger with sentitive defaults

**Usage:**
```
#!javascript
var express = require('express');
var app = express(); 

global.log = require('mantra-logger')(options); // options are optional ;)

// express HTTP logger
app.use(log.http);

// now you can log everywhere like this
log.info('some info');

// same as
log('info', 'some info');

```
If no options are passed sensitive defaults are applied. All winston options are supported

default log levels are: ['debug', 'info', 'warn', 'error']