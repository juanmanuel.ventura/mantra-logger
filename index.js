/** Created by Juan Manuel Ventura */

'use strict';

var winston = require('winston');
var morgan = require('morgan');
var defaults = require('./config');

module.exports = function (config) {
  config = config ? Object.extend(defaults, config) : defaults;

  var logger = new winston.Logger(config.server.defaults);
  logger.add(winston.transports.Console, config.server.transports.console);
  logger.add(winston.transports.File, config.server.transports.file);

  var http = new winston.Logger(config.http.defaults);
  http.add(winston.transports.Console, config.http.transports.console);
  http.add(winston.transports.File, config.http.transports.file);

  logger.http = morgan(config.http.format, {
    stream: {
      write: function (message) {
        http.log('http', message.replace(/\s+/g, ' ').trim());
      }
    }
  });

  return logger;
};

Object.extend = function (destination, source) {
  for (var property in source) {
    if (source.hasOwnProperty(property)) destination[property] = source[property];
  }
  return destination;
};
